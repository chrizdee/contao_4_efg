<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license LGPL-3.0+
 * @copyright Thomas Kuhn 2007-2015
 */


/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['MSC']['efgTotalNumberOfItems'] = 'Number of items: %s';
