<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license LGPL-3.0+
 * @copyright Thomas Kuhn 2007-2015
 */


// This file is created when saving a form in form generator
// last created on 2018-10-19 19:19:18


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['formdata'] = 'Form data';
$GLOBALS['TL_LANG']['MOD']['efg'] = 'Form data';
$GLOBALS['TL_LANG']['MOD']['feedback'] = array('All results', 'Stored data from forms.');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-simple'] = array('Kurzkontakt-Vineria', 'Stored data from form "Kurzkontakt-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-vineria'] = array('Kontakt-Vineria', 'Stored data from form "Kontakt-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_eventanfrage-vineria'] = array('Eventanfrage-Vineria', 'Stored data from form "Eventanfrage-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_hochzeitsanfrage-vineria'] = array('Hochzeitsanfrage-Vineria', 'Stored data from form "Hochzeitsanfrage-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_tagungsanfrage-vineria'] = array('Tagungsanfrage-Vineria', 'Stored data from form "Tagungsanfrage-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_gutscheinbestellung-vineria'] = array('Gutscheinbestellung-Vineria', 'Stored data from form "Gutscheinbestellung-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_reservierung-vineria'] = array('Reservierung-Vineria', 'Stored data from form "Reservierung-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_katalogbestellung'] = array('Katalogbestellung', 'Stored data from form "Katalogbestellung".');
$GLOBALS['TL_LANG']['MOD']['fd_katalogdownload'] = array('Katalogdownload', 'Stored data from form "Katalogdownload".');
$GLOBALS['TL_LANG']['MOD']['fd_reservierung-vineria-mit-entfernungs-check-v2'] = array('Reservierung-Vineria (mit Entfernungs-Check)-V2', 'Stored data from form "Reservierung-Vineria (mit Entfernungs-Check)-V2".');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['formdatalisting'] = array('Listing form data', 'Use this module to list the records of a certain form data table in the front end.');
