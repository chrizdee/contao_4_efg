<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license LGPL-3.0+
 * @copyright Thomas Kuhn 2007-2015
 */


// This file is created when saving a form in form generator
// last created on 2018-10-19 19:19:18


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['formdata'] = 'Formular-Daten';
$GLOBALS['TL_LANG']['MOD']['efg'] = 'Formular-Daten';
$GLOBALS['TL_LANG']['MOD']['feedback'] = array('Feedback', 'Gespeicherte Daten aus Formularen.');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-simple'] = array('Kurzkontakt-Vineria', 'Gespeicherte Daten aus Formular "Kurzkontakt-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_kontakt-vineria'] = array('Kontakt-Vineria', 'Gespeicherte Daten aus Formular "Kontakt-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_eventanfrage-vineria'] = array('Eventanfrage-Vineria', 'Gespeicherte Daten aus Formular "Eventanfrage-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_hochzeitsanfrage-vineria'] = array('Hochzeitsanfrage-Vineria', 'Gespeicherte Daten aus Formular "Hochzeitsanfrage-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_tagungsanfrage-vineria'] = array('Tagungsanfrage-Vineria', 'Gespeicherte Daten aus Formular "Tagungsanfrage-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_gutscheinbestellung-vineria'] = array('Gutscheinbestellung-Vineria', 'Gespeicherte Daten aus Formular "Gutscheinbestellung-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_reservierung-vineria'] = array('Reservierung-Vineria', 'Gespeicherte Daten aus Formular "Reservierung-Vineria".');
$GLOBALS['TL_LANG']['MOD']['fd_katalogbestellung'] = array('Katalogbestellung', 'Gespeicherte Daten aus Formular "Katalogbestellung".');
$GLOBALS['TL_LANG']['MOD']['fd_katalogdownload'] = array('Katalogdownload', 'Gespeicherte Daten aus Formular "Katalogdownload".');
$GLOBALS['TL_LANG']['MOD']['fd_reservierung-vineria-mit-entfernungs-check-v2'] = array('Reservierung-Vineria (mit Entfernungs-Check)-V2', 'Gespeicherte Daten aus Formular "Reservierung-Vineria (mit Entfernungs-Check)-V2".');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['formdatalisting'] = array('Auflistung Formular-Daten', 'Verwenden Sie dieses Modul dazu, die Daten einer beliebigen Formular-Daten-Tabelle im Frontend aufzulisten.');
