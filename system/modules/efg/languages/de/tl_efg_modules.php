<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   Efg
 * @author    Thomas Kuhn <mail@th-kuhn.de>
 * @license LGPL-3.0+
 * @copyright Thomas Kuhn 2007-2015
 */


$GLOBALS['TL_LANG']['MOD']['formdata'] = 'Formular-Daten';
$GLOBALS['TL_LANG']['MOD']['efg'] = 'Formular-Daten';
$GLOBALS['TL_LANG']['MOD']['feedback'] = array('Feedback', 'Gespeicherte Daten aus Formularen.');
$GLOBALS['TL_LANG']['MOD']['formdata_from'] = 'Gespeicherte Daten aus Formular';
$GLOBALS['TL_LANG']['MOD']['formdatalisting'] = array('Auflistung Formular-Daten', 'Mit diesem Modul können Sie eine beliebige Formular-Daten-Tabelle im Frontend auflisten.');
$GLOBALS['TL_LANG']['FMD']['formdatalisting'] = array('Auflistung Formular-Daten', 'Verwenden Sie dieses Modul dazu, die Daten einer beliebigen Formular-Daten-Tabelle im Frontend aufzulisten.');
